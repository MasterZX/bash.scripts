#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

#Install Apache HTTP server and required modules
apt-get install subversion libapache2-svn apache2

#Enable SSL
a2enmod ssl

#Generate an SSL certificate
apt-get install ssl-cert
mkdir /etc/apache2/ssl
/usr/sbin/make-ssl-cert /usr/share/ssl-cert/ssleay.cnf /etc/apache2/ssl/apache-svn.pem

#Create virtual host
cp /etc/apache2/sites-available/default /etc/apache2/sites-available/svnserver

#Change (in ports.conf):
#sed -i 's/old-word/new-word/g' ports.conf
#"NameVirtualHost *" to "NameVirtualHost *:443"
#and (in svnserver)
#<VirtualHost *> to <VirtualHost *:443>
#Add, under ServerAdmin (also in file svnserver):
#SSLEngine on
#SSLCertificateFile /etc/apache2/ssl/apache.pem
#SSLProtocol all
#SSLCipherSuite HIGH:MEDIUM


#Enable the site:
a2ensite svnserver
service apache2 restart