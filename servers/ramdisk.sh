#!/bin/bash

#
# Create RamDisk with TmpFS
#
#

RAMDISK=/var/ramdisk/current
RAMDISK_ARCHIVE=/var/ramdisk/archive
RAMDISK_SIZE=5000M
RAMDISK_SHUTDOWN=/etc/init.d/ramdisk-sync-shutdown
CRON_SYNC='0 * * * *'

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

mkdir $RAMDISK
mkdir $RAMDISK_ARCHIVE

#sudo gedit /etc/fstab
cp /etc/fstab /etc/fstab.noramdisk
sed -n '/tmpfs $RAMDISK/!p' /etc/fstab
sed -i -e 'tmpfs $RAMDISK tmpfs defaults,size=$RAMDISK_SIZE,mode=1777 0 0' /etc/fstab/

#sudo gedit /etc/init.d/ramdisk-sync-shutdown
echo "#!/bin/sh"                           > $RAMDISK_SHUTDOWN
echo "rsync -a $RAMDISK $RAMDISK_ARCHIVE" >> $RAMDISK_SHUTDOWN
echo "exit 0"                             >> $RAMDISK_SHUTDOWN

#sudo gedit /etc/init.d/rc.local
cp /etc/init.d/rc.local /etc/init.d/rc.local.noramdisk
sed -n '/rsync -a $RAMDISK_ARCHIVE/!p' /etc/init.d/rc.local
sed -i -e 'rsync -a $RAMDISK_ARCHIVE $RAMDISK' /etc/init.d/rc.local
#rsync -a $RAMDISK_ARCHIVE $RAMDISK


chmod +x $RAMDISK_SHUTDOWN

ln -s $RAMDISK_SHUTDOWN /etc/rc0.d/S00ramdisk-sync-shutdown
ln -s $RAMDISK_SHUTDOWN /etc/rc1.d/S00ramdisk-sync-shutdown
ln -s $RAMDISK_SHUTDOWN /etc/rc6.d/S00ramdisk-sync-shutdown

crontab 0 * * * * $RAMDISK_SHUTDOWN

mount -a
