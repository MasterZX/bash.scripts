#!/bin/bash

#
# Create Git server
# http://habrahabr.ru/post/43806/
#

GIT_WWW=git.domain
GIT_WEBCONF=/var/www/$GIT_WWW/gitweb.conf
GIT_USER=MasterZX
GIT_A2SITE=/etc/apache2/sites-available/gitweb.conf


if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

OS=$(lsb_release -si)
if [ $OS != 'Ubuntu' ]
  then echo "For Ubuntu only"
  exit
fi

#Install Git server and required modules
apt-get update
apt-get -y upgrade
apt-get -y install apache2 apache2-utils ssh git-core git-svn gitweb libapache2-mod-perl2

mkdir -p /var/www/$GIT_WWW/{htdocs,logs,img,css} /var/www/$GIT_WWW/htdocs/git
cp /usr/share/gitweb/static/git-logo.png /var/www/$GIT_WWW/img/git-logo.png
cp /usr/share/gitweb/static/git-favicon.png /var/www/$GIT_WWW/img/git-favicon.png
cp /usr/share/gitweb/static/gitweb.css /var/www/$GIT_WWW/css/gitweb.css

a2enmod dav
a2enmod dav_fs
a2enmod rewrite
a2enmod env
a2enmod cgi

echo "\$my_uri = \"http://$GIT_WWW/git/\";"                     > $GIT_WEBCONF
echo "\$site_name = \"$GIT_WWW\";"                        >> $GIT_WEBCONF
echo "\$projectroot = \"/var/www/$GIT_WWW/htdocs/git/\";" >> $GIT_WEBCONF
echo '$git_temp = "/tmp";'                             >> $GIT_WEBCONF
echo '$home_link = $my_uri;'                           >> $GIT_WEBCONF
echo '$projects_list = $projectroot;'                  >> $GIT_WEBCONF
echo "\$stylesheet = \"/var/www/$GIT_WWW/gitweb.css\";" >> $GIT_WEBCONF
echo "\$logo = \"/var/www/$GIT_WWW/img/git-logo.png\";"        >> $GIT_WEBCONF
echo "\$favicon = \"/var/www/$GIT_WWW/img/git-favicon.png\";"  >> $GIT_WEBCONF
echo '$projects_list_description_width = 40;'          >> $GIT_WEBCONF
echo '$feature{'pathinfo'}{'default'} = [1];'          >> $GIT_WEBCONF


echo '<VirtualHost *:80>'                            > $GIT_A2SITE
echo "  ServerName $GIT_WWW"                        >> $GIT_A2SITE
echo "  ServerAlias www.$GIT_WWW"                   >> $GIT_A2SITE
echo "  ServerAdmin head@$GIT_WWW"                  >> $GIT_A2SITE
echo "  DocumentRoot /var/www/$GIT_WWW/htdocs"      >> $GIT_A2SITE
echo '  ScriptAlias /cgi-bin/ /usr/share/gitweb/'   >> $GIT_A2SITE
echo '  DirectoryIndex /cgi-bin/gitweb.cgi'         >> $GIT_A2SITE
echo '  RewriteEngine on'                           >> $GIT_A2SITE
echo '  RewriteRule ^/$  /cgi-bin/gitweb.cgi'       >> $GIT_A2SITE
echo '  RewriteRule ^/(.*\.git/(?!/?(HEAD|info|objects|refs)).*)?$ /cgi-bin/gitweb.cgi%{REQUEST_URI}  [L,PT]'  >> $GIT_A2SITE
#echo '  RewriteRule ^/([a-zA-Z0-9_\-]+\/\.git)/?(\?.*)?$ /cgi-bin/gitweb.cgi/$1 [L,PT]'  >> $GIT_A2SITE
echo "  SetEnv GITWEB_CONFIG $GIT_WEBCONF"          >> $GIT_A2SITE
echo '  Alias /gitweb /usr/share/gitweb/'           >> $GIT_A2SITE
echo ''                                             >> $GIT_A2SITE
echo "  <Directory /var/www/$GIT_WWW/htdocs>"       >> $GIT_A2SITE
echo '    Options FollowSymLinks'                   >> $GIT_A2SITE
echo '    AllowOverride None'                       >> $GIT_A2SITE
echo '    Order allow,deny'                         >> $GIT_A2SITE
echo '    allow from all'                           >> $GIT_A2SITE
#echo '    RewriteEngine On'                         >> $GIT_A2SITE
#echo '    RewriteCond %{REQUEST_FILENAME} !-f'      >> $GIT_A2SITE
#echo '    RewriteCond %{REQUEST_FILENAME} !-d'      >> $GIT_A2SITE
#echo '    RewriteRule ^.* /gitweb.cgi/$0 [L,PT]'    >> $GIT_A2SITE
echo '  </Directory>'                               >> $GIT_A2SITE
echo ''                                             >> $GIT_A2SITE
#echo '  <Location /git>'                            >> $GIT_A2SITE
#echo '    DAV on'                                   >> $GIT_A2SITE
#echo '    AuthType Basic'                           >> $GIT_A2SITE
#echo '    AuthName «Git»'                           >> $GIT_A2SITE
#echo "    AuthUserFile /var/www/$GIT_WWW/passwd.git"          >> $GIT_A2SITE
#echo '    <LimitExcept GET HEAD PROPFIND OPTIONS REPORT>'     >> $GIT_A2SITE
#echo '       Require valid-user'                              >> $GIT_A2SITE
#echo '    </LimitExcept>'                                     >> $GIT_A2SITE
#echo '  </Location>'                                          >> $GIT_A2SITE
echo ''                                                       >> $GIT_A2SITE
echo '  LogLevel debug'                                        >> $GIT_A2SITE
echo "  ErrorLog  /var/www/$GIT_WWW/logs/error.log"           >> $GIT_A2SITE
echo "  CustomLog /var/www/$GIT_WWW/logs/access.log combined" >> $GIT_A2SITE
echo '</VirtualHost>'                                         >> $GIT_A2SITE

htpasswd -cm /var/www/$GIT_WWW/passwd.git $GIT_USER
chown www-data:www-data /var/www/$GIT_WWW/ -R
chmod g+rw /var/www/$GIT_WWW/ -R

a2dissite 000-default.conf
a2ensite gitweb.conf

service apache2 restart
