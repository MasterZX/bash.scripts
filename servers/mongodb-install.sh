#!/bin/bash

#
# Create MongoDB server with phpMoAdmin
#

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

OS=$(lsb_release -si)
if [ $OS != 'Ubuntu' ]
  then echo "For Ubuntu only"
  exit
fi

#Install MongoDB server and required modules
apt-get update
apt-get -y upgrade
apt-get -y install apache2 apache2-utils ssh mongodb libapache2-mod-php5 git php5-mongo

cd ~
git clone https://github.com/MongoDB-Rox/phpMoAdmin-MongoDB-Admin-Tool-for-PHP.git
mkdir -p /var/www/phpnoadmin/
cp phpMoAdmin-MongoDB-Admin-Tool-for-PHP/moadmin.php /var/www/phpnoadmin/index.php
chown www-data:www-data /var/www/phpnoadmin/ -R
rm -rf phpMoAdmin-MongoDB-Admin-Tool-for-PHP/

service apache2 restart