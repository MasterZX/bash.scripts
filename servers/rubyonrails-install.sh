#!/bin/bash

#
# Create RubyOnRails server
# http://habrahabr.ru/post/43806/
#

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

OS=$(lsb_release -si)
if [ $OS != 'Ubuntu' ]
  then echo "For Ubuntu only"
  exit
fi

#Install Git server and required modules
add-apt-repository ppa:brightbox/ruby-ng
apt-get update
apt-get -y upgrade
apt-get -y install apache2 apache2-utils ssh ruby2.1 git libapache2-mod-passenger rails apt-get install ruby-dev libfcgi libfcgi-dev git
gem install fcgi
gem install bundler

a2enmod rewrite

service apache2 restart
