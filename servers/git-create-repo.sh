#!/bin/bash

function msg_help {
  echo 'Script create empty remote Git repository.'
  echo 'usage: git-create-repo.sh servername reponame'
}

if [[ "$1" == "-h" ]] ; then
    msg_help
    exit 1
fi

if [[ "$1" == "" ]] ; then
    echo 'Servername is empty. Example: git.myprojects.org'
    msg_help
    exit 1
fi

if [[ "$2" == "" ]] ; then
    echo 'Reponame is empty. Example: myproject'
    msg_help
    exit 1
fi

GIT_WWW="$1"
GIT_REPO="$2"

mkdir -p /var/www/$GIT_WWW/htdocs/git/$GIT_REPO.git
cd /var/www/$GIT_WWW/htdocs/git/$GIT_REPO.git
git init --bare

chown www-data:www-data /var/www/$GIT_WWW/htdocs/git/ -R