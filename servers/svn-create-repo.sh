#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

mkdir /var/svn

REPOS=$1
svnadmin create /var/svn/$REPOS
chown -R www-data:www-data /var/svn/$REPOS
chmod -R g+ws /var/svn/$REPOS